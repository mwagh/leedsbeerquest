﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Elasticsearch.Net;
using LeedsBeerQuest.ETL.Models;
using Nest;

namespace LeedsBeerQuest.ETL.Services
{
    public class ElasticsearchService
    {
        private readonly IElasticClient _client;

        public ElasticsearchService(IElasticClient client)
        {
            _client = client;
        }

        public void CreateIndex()
        {
            Console.WriteLine($"Creating index: venues");
            var response = _client.Indices.Create("venues", c => c
                .Map(x => 
                    x.AutoMap<Venue>()
                )
            );

            Console.WriteLine($"response: {response.DebugInformation}");
            if (!response.IsValid)
            {
                throw new Exception(response.OriginalException.Message);
            }
        }

        public void InsertData(IEnumerable<Venue> venues)
        {
            var response = _client.IndexMany(venues, "venues");

            if (!response.IsValid)
            {
                throw new Exception(response.OriginalException.Message);
            }
        }
    }
}