﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using CsvHelper;
using LeedsBeerQuest.ETL.Models;

namespace LeedsBeerQuest.ETL
{
    public static class CSVParser
    {
        public static IEnumerable<Venue> ReadCsv(string filePath)
        {
            IEnumerable<Venue> records = new Venue[]{};
            
            using (var reader = new StreamReader(filePath))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                csv.Context.RegisterClassMap<VenueMap>();
                records = csv.GetRecords<Venue>().ToList();
            }

            return records;
        }
    }
}