﻿using System;

namespace LeedsBeerQuest.ETL.Models
{
    public class Venue
    {
        public string Name { get; set; }
        public string Category { get; set; }
        public string Url { get; set; }
        public DateTime Date { get; set; }
        public string Excerpt { get; set; }
        public string Thumbnail { get; set; }
        public string Lat { get; set; }
        public string Lng { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Twitter { get; set; }
        public decimal StarsBeer { get; set; }
        public decimal StarsAtmosphere { get; set; }
        public decimal StarsAmenities { get; set; }
        public decimal StarsValue { get; set; }
        public string Tags { get; set; }
    }
}