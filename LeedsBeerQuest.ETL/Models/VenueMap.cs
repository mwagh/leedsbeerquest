﻿using CsvHelper.Configuration;

namespace LeedsBeerQuest.ETL.Models
{
    public class VenueMap: ClassMap<Venue>
    {
        public VenueMap()
        {
            Map(m => m.Address).Name("address");
            Map(m => m.Category).Name("category");
            Map(m => m.Date).Name("date");
            Map(m => m.Excerpt).Name("excerpt");
            Map(m => m.Lat).Name("lat");
            Map(m => m.Lng).Name("lng");
            Map(m => m.Name).Name("name");
            Map(m => m.Phone).Name("phone");
            Map(m => m.Tags).Name("tags");
            Map(m => m.Thumbnail).Name("thumbnail");
            Map(m => m.Twitter).Name("twitter");
            Map(m => m.Url).Name("url");
            Map(m => m.StarsAmenities).Name("stars_amenities");
            Map(m => m.StarsAtmosphere).Name("stars_atmosphere");
            Map(m => m.StarsBeer).Name("stars_beer");
            Map(m => m.StarsValue).Name("stars_value");
        }
    }
}