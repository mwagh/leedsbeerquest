﻿using System;
using System.Linq;
using Elasticsearch.Net;
using LeedsBeerQuest.ETL.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nest;

namespace LeedsBeerQuest.ETL
{
    class Program
    {
        static void Main(string[] args)
        {
            IConfiguration config = new ConfigurationBuilder()
                .AddJsonFile("./appsettings.json", false, true)
                .Build();
            
            var serviceProvider = new ServiceCollection()
                .AddSingleton<IElasticClient, ElasticClient>(x =>
                {
                    Console.WriteLine($"Creating elasticsearch client with url: {config["ElasticsearchUrl"]}");
                    var pool = new SingleNodeConnectionPool(new Uri(config["ElasticsearchUrl"]));
                    return new ElasticClient(new ConnectionSettings(pool));
                })
                .BuildServiceProvider();

            var data = CSVParser.ReadCsv(config["FilePath"]);
            var service = new ElasticsearchService(serviceProvider.GetService<IElasticClient>());
            service.CreateIndex();
            service.InsertData(data);
            Console.WriteLine($"Got: {data.Count()} rows");
        }
    }
}