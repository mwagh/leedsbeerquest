﻿using System.Threading.Tasks;
using LeedsBeerQuest.Web.Controllers;
using LeedsBeerQuest.Web.Models.Requests;
using LeedsBeerQuest.Web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NSubstitute;
using Xunit;

namespace LeedsBeerQuest.Web.Tests.Controllers
{
    public class VenuesControllerTests
    {
        private readonly VenueController _subject;
        private readonly IElasticsearchService _elasticsearchServiceMock;
        private readonly ILogger<VenueController> _loggerMock;

        public VenuesControllerTests()
        {
            _loggerMock = Substitute.For<ILogger<VenueController>>();
            _elasticsearchServiceMock = Substitute.For<IElasticsearchService>();
            _subject = new VenueController(_loggerMock, _elasticsearchServiceMock);
        }

        [Fact]
        public async Task GetVenues_WithValidParams_ReturnsOk()
        {
            var response = await _subject.GetVenues(new GetVenuesRequest());
            Assert.IsType<OkObjectResult>(response);
        }

        [Fact]
        public async Task GetVenues_WithInvalidParams_ReturnsBadRequest()
        {
            _subject.ModelState.AddModelError("test key", "test error");
            var response = await _subject.GetVenues(new GetVenuesRequest
            {
                From = -1,
                SortValue = "fail"
            });
            Assert.IsType<BadRequestResult>(response);
        }
    }
}