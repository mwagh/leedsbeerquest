﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using LeedsBeerQuest.Web.Models;
using LeedsBeerQuest.Web.Services;
using Nest;
using NSubstitute;
using Xunit;

namespace LeedsBeerQuest.Web.Tests.Services
{
    public class ElasticsearchServiceTests
    {
        private readonly IElasticClient _elasticClientMock;
        private readonly ElasticsearchService _subject;

        public ElasticsearchServiceTests()
        {
            _elasticClientMock = Substitute.For<IElasticClient>();
            _subject = new ElasticsearchService(_elasticClientMock);
        }

        [Fact]
        public async Task GetVenues_WithDefaultParams_ReturnsData()
        {
            var expectedVenue = new Venue
            {
                Name = "test venue"
            };

            var docs = new List<Venue> { expectedVenue };

            var expectedResponse = Substitute.For<ISearchResponse<Venue>>();
            expectedResponse.Documents.Returns(new ReadOnlyCollection<Venue>(docs));

            _elasticClientMock
                .SearchAsync<Venue>()
                .ReturnsForAnyArgs(expectedResponse);

            var result = await _subject.GetVenues(1, 10, null, null);
            Assert.NotEmpty(result.Venues);
            Assert.Equal(expectedVenue.Name, result.Venues.First().Name);
        }
        
        [Fact]
        public async Task GetVenues_WithValidParams_BuildsCorrectQuery_AndReturnsData()
        {
            var expectedVenue = new Venue
            {
                Name = "test venue"
            };

            var docs = new List<Venue> { expectedVenue };

            var expectedResponse = Substitute.For<ISearchResponse<Venue>>();
            expectedResponse.Documents.Returns(new ReadOnlyCollection<Venue>(docs));

            _elasticClientMock
                .SearchAsync<Venue>()
                .ReturnsForAnyArgs(expectedResponse);

            var result = await _subject.GetVenues(12, 34, "testfield", "desc");
            _elasticClientMock
                .Received()
                .SearchAsync(Arg.Any<Func<SearchDescriptor<Venue>, ISearchRequest>>());
        }
    }
}