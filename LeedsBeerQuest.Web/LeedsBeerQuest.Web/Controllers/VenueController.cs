﻿using System.Threading.Tasks;
using LeedsBeerQuest.Web.Models.Requests;
using LeedsBeerQuest.Web.Models.Responses;
using LeedsBeerQuest.Web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace LeedsBeerQuest.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VenueController : ControllerBase
    {
        private readonly ILogger<VenueController> _logger;
        private IElasticsearchService _client;

        public VenueController(ILogger<VenueController> logger, IElasticsearchService client)
        {
            _logger = logger;
            _client = client;
        }
        
        [HttpGet]
        public async Task<ActionResult<GetVenuesResponse>> GetVenues([FromQuery] GetVenuesRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            
            var venues = await _client.GetVenues(request.From, request.Size, request.SortField, request.SortValue);
            return Ok(venues);
        }
    }
}