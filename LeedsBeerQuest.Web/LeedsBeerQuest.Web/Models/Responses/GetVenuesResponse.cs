﻿using System.Collections.Generic;

namespace LeedsBeerQuest.Web.Models.Responses
{
    public class GetVenuesResponse
    {
        public IEnumerable<Venue> Venues { get; set; }
        public long Total { get; set; }
    }
}