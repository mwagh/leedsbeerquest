﻿using System.ComponentModel.DataAnnotations;

namespace LeedsBeerQuest.Web.Models.Requests
{
    public class GetVenuesRequest
    {
        [Range(0, int.MaxValue)]
        public int From { get; set; }
        [Range(1, int.MaxValue)]
        public int? Size { get; set; }
        public string SortField { get; set; }
        [RegularExpression("(asc|desc)")]
        public string SortValue { get; set; }
    }
}