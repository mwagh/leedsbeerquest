﻿using System.Threading.Tasks;
using LeedsBeerQuest.Web.Models.Responses;

namespace LeedsBeerQuest.Web.Services
{
    public interface IElasticsearchService
    {
        Task<GetVenuesResponse> GetVenues(int? from, int? size, string sortField, string sortValue);
    }
}