﻿using System.Threading.Tasks;
using LeedsBeerQuest.Web.Models;
using LeedsBeerQuest.Web.Models.Responses;
using Nest;

namespace LeedsBeerQuest.Web.Services
{
    public class ElasticsearchService: IElasticsearchService
    {
        private readonly IElasticClient _client;
        private const string DefaultSortField = "name.keyword";

        public ElasticsearchService(IElasticClient client)
        {
            _client = client;
        }
        public async Task<GetVenuesResponse> GetVenues(int? from, int? size, string sortField, string sortValue)
        {
            System.Console.WriteLine($"Getting venues");
            var sortOrder = sortValue == "desc"
                    ? SortOrder.Descending
                    : SortOrder.Ascending;
                
            var searchResponse = await _client.SearchAsync<Venue>(s => s
                .Index("venues")
                .Query(q => q
                    .MatchAll()
                )
                .From(from)
                .Sort(descriptor => descriptor.Field(sortField ?? DefaultSortField, sortOrder))
                .Size(size ?? 10)
            );

            System.Console.WriteLine($"Response: {searchResponse.ApiCall.DebugInformation}");
            return new GetVenuesResponse
            {
                Total = searchResponse.Total,
                Venues = searchResponse.Documents
            };
        }
    }
}