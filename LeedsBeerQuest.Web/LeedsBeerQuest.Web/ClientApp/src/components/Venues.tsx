﻿import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { ApplicationState } from '../store';
import * as VenueStore from '../store/Venues';
import "./Venues.css";
import { ChangeEvent } from "react";

type VenueProps =
    VenueStore.VenueState &
    typeof VenueStore.actionCreators &
    RouteComponentProps;

class Venue extends React.PureComponent<VenueProps> {
    setSize(event: ChangeEvent<HTMLSelectElement>) {
        this.props.setSize(parseInt(event.target.value));
        this.props.requestVenues();
    }

    setSortField(event: ChangeEvent<HTMLSelectElement>) {
        this.props.setSortField(event.target.value);
        this.props.requestVenues();
    }

    setSortValue(event: ChangeEvent<HTMLSelectElement>) {
        this.props.setSortValue((event.target.value) as any);
        this.props.requestVenues();
    }
    
    incrementPage() {
        this.props.setFrom(this.props.from+this.props.size);
        this.props.requestVenues();
    }

    decrementPage() {
        var newFrom = this.props.from-this.props.size;
        if (newFrom < 1) {
            newFrom = 1;
        }
        this.props.setFrom(newFrom);
        this.props.requestVenues();
    }

    canIncrementPage() {
        return this.props.from < this.props.totalVenues;
    }

    canDecrementPage() {
        return this.props.from > 1;
    }
    
    public render() {
        return (
            <React.Fragment>
                <h1>Venues</h1>
                {this.props.totalVenues > 0 &&
                    (<p>Showing venues {this.props.from}-{this.props.from+this.props.size-1} of {this.props.totalVenues}</p>)
                }
                <div>
                    {this.canDecrementPage() && <button type={"button"} onClick={() => this.decrementPage()}>previous</button>}
                    {this.canIncrementPage() && <button type={"button"} onClick={() => this.incrementPage()}>next</button>}
                </div>
                <div className={"filter-container"}>
                    <div className={"filter-page-size"}>
                        <p>Results Per Page</p>
                        <select value={this.props.size} onChange={(event) => this.setSize(event)}>
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="40">40</option>
                            <option value="100">100</option>
                        </select>
                    </div>

                    <div className={"filter-page-size"}>
                        <p>Sort By</p>
                        <select value={this.props.sortField} onChange={(event) => this.setSortField(event)}>
                            <option value="starsAmenities">Amenities</option>
                            <option value="starsAtmosphere">Atmosphere</option>
                            <option value="starsBeer">Beer</option>
                            <option value="starsValue">Value</option>
                        </select>
                        <select value={this.props.sortValue} onChange={(event) => this.setSortValue(event)}>
                            <option value="asc">Ascending</option>
                            <option value="desc">Descending</option>
                        </select>
                    </div>
                </div>
                <button type="button"
                        className="btn btn-primary btn-lg"
                        onClick={() => { this.props.requestVenues(); }}>
                    Search Venues
                </button>
                <div className={"venues-list"}>
                    {this.props.venues && this.props.venues.map(venue => (
                        <div className={"venue-item"}>
                            <div className={"venue-top"}>
                                <div className={"venue-info"}>
                                    <p>{venue.name}</p>
                                    <p>{venue.excerpt}</p>
                                </div>
                                <a href={venue.url}>
                                    <img height={200} width={200} src={venue.thumbnail}/>
                                </a>
                            </div>
                            <div className={"venue-ratings"}>
                                <p>Amenities: {venue.starsAmenities}/5</p>
                                <p>Atmosphere: {venue.starsAtmosphere}/5</p>
                                <p>Beer: {venue.starsBeer}/5</p>
                                <p>Value: {venue.starsValue}/5</p>
                            </div>
                        </div>
                    ))}
                </div>
            </React.Fragment>
        );
    }
};

export default connect((state: ApplicationState) => state.venues, VenueStore.actionCreators)(Venue as any)
