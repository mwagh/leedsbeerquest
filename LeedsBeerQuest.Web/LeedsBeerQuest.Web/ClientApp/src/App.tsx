import * as React from 'react';
import { Route } from 'react-router';
import Layout from './components/Layout';
import Venues from "./components/Venues";

import './custom.css'

export default () => (
    <Layout>
        <Route exact path='/' component={Venues} />
    </Layout>
);
