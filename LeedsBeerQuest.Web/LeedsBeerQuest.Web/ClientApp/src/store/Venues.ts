import { Action, Reducer } from 'redux';
import {Venue} from "../Models/Venue";
import {AppThunkAction} from "./index";
import {GetVenuesResponse} from "../Models/GetVenuesResponse";

export interface VenueState {
    from: number;
    size: number;
    sortField: string;
    sortValue: "asc" | "desc";
    venues: Venue[];
    totalVenues: number;
    isLoading: boolean;
}

export interface SetFromAction { type: 'SET_FROM', from: number }
export interface SetSizeAction { type: 'SET_SIZE', size: number }
export interface SetSortFieldAction { type: 'SET_SORT_FIELD', sortField: string }
export interface SetSortValueAction { type: 'SET_SORT_VALUE', sortValue: "asc" | "desc" }
export interface RequestVenuesAction { type: 'REQUEST_VENUES' }
export interface ReceiveVenuesAction { type: 'RECEIVE_VENUES', venues: GetVenuesResponse }

export type KnownAction = SetFromAction | SetSizeAction | SetSortFieldAction | SetSortValueAction | RequestVenuesAction | ReceiveVenuesAction;

export const actionCreators = {
    setSize: (size: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({type: "SET_SIZE", size})
    },
    setFrom: (from: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({type: "SET_FROM", from})
    },
    setSortField: (sortField: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({type: "SET_SORT_FIELD", sortField})
    },
    setSortValue: (sortValue: "asc" | "desc"): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({type: "SET_SORT_VALUE", sortValue})
    },
    requestVenues: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        const state = getState().venues;
        if (state) {
            fetch(`api/venue?from=${state.from}&size=${state.size}&sortField=${state.sortField}&sortValue=${state.sortValue}`)
                .then(response => response.json() as Promise<GetVenuesResponse>)
                .then(data => {
                    dispatch({type: 'RECEIVE_VENUES', venues: data});
                });

            dispatch({type: 'REQUEST_VENUES'});
        }
    },
};

export const reducer: Reducer<VenueState> = (state: VenueState | undefined, incomingAction: Action): VenueState => {
    if (state === undefined) {
        return {
            size: 10,
            from: 1,
            sortField: "name.keyword",
            sortValue: "asc",
            venues: [],
            totalVenues: 0,
            isLoading: false
        };
    }

    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'SET_SIZE':
            return {
                ...state,
                size: action.size
            }
        case 'SET_FROM':
            return {
                ...state,
                from: action.from
            }
        case 'SET_SORT_FIELD':
            return {
                ...state,
                sortField: action.sortField
            }
        case 'SET_SORT_VALUE':
            return {
                ...state,
                sortValue: action.sortValue
            }
        case 'REQUEST_VENUES':
            return {
                ...state,
                isLoading: true
            };
        case 'RECEIVE_VENUES':
            return {
                ...state,
                venues: action.venues.venues,
                totalVenues: action.venues.total,
                isLoading: false
            };
        default:
            return state;
    }
};
