﻿import {Venue} from "./Venue";

export interface GetVenuesResponse {
    total: number,
    venues: Venue[]
}