export interface Venue {
    name: string;
    category: string;
    url: string;
    date: Date;
    excerpt: string;
    thumbnail: string;
    lat: string;
    lng: string;
    address: string;
    phone: string;
    twitter: string;
    starsBeer: number;
    starsAtmosphere: number;
    starsAmenities: number;
    starsValue: number;
    tags: string;
}